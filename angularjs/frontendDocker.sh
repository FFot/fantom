#!/bin/bash

echo "const AppConstants = {

  api: 'http://api.${DNS_PREFIX}.${DOMAIN_NAME}',

  appName: 'codejar',
};

export default AppConstants;" >src/js/config/app.constants.js

docker build -t frontend:latest -f Dockerfile.angularjs .
docker run -itd --name Frontend -p 80:80 frontend
