#!/bin/bash

echo "const AppConstants = {

  api: 'http://api.${DNS_PREFIX}.${DOMAIN_NAME}',

  appName: 'codejar',
};

export default AppConstants;" >src/js/config/app.constants.js

repoUri=$(aws ecr create-repository --repository-name fantom/frontend --region $AWS_DEFAULT_REGION | jq -r '.repository.repositoryUri')
aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin $repoUri
docker build -t fantom/frontend:latest -f Dockerfile.angularjs .
docker tag fantom/frontend:latest ${repoUri}:latest
docker push ${repoUri}:latest
