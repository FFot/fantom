import marked from 'marked';

class ArticleCtrl {
  constructor(article, User, Comments, $sce, $rootScope,$http) {
    'ngInject';

    this.article = article;
    this._Comments = Comments;
    this._$http=$http
    this.readmedata=""
    this.gitUrl=article.gits[0].git_url
    this.showReadMe=false;

   
    
    
    this.currentUser = User.current;
    this.loadData =function($http) {
      this._$http({
        url: `https://api.github.com/repos${this.gitUrl}/readme`,
        method: 'GET',
  
        headers: {
          "Accept": "application/vnd.github.v3.raw"
        }
      }).then(
        
       res=>{
        var converter = new showdown.Converter();
        
var data=converter.makeHtml(res.data)

        this.readmedata=$sce.trustAsHtml(data)
        this.showReadMe=true;
    
       }
      );
  
    }
    if(this.gitUrl.indexOf("github.com/")>-1){
      this.gitUrl=article.gits[0].git_url.slice(this.gitUrl.indexOf('github.com/')+10)
      console.log(this.gitUrl)
      this.loadData()
    }
   
    // getReadme()


    console.log(this.article)
    // $rootScope.setPageTitle(this.article.gits[0].description);

    // this.article.body = $sce.trustAsHtml(marked(this.article.body, { sanitize: true }));


    // Comments.getAll(this.article.slug).then(
    //   (comments) => this.comments = comments
    // );

    this.resetCommentForm();
  }

  resetCommentForm() {
    this.commentForm = {
      isSubmitting: false,
      body: '',
      errors: []
    }
  }
  getReadme() {
 return this._$http({
      url: 'https://api.github.com/repos/slimphp/Slim/readme',
      method: 'GET',

      headers: {
        "Accept": "application/vnd.github.v3.raw"
      }
    }).then(
     res=>console.log(res.data)

  
    );

}




  addComment(){
    this.commentForm.isSubmitting = true;

    this._Comments.add(this.article.slug, this.commentForm.body).then(
      (comment) => {
        this.comments.unshift(comment);
        this.resetCommentForm();
      },
      (err) => {
        this.commentForm.isSubmitting = false;
        this.commentForm.errors = err.data.errors;
      }
    )
  }

  deleteComment(commentId, index) {
    this._Comments.destroy(commentId, this.article.slug).then(
      (success) => {
        this.comments.splice(index, 1);
      }
    )
  }
  
}


export default ArticleCtrl;
