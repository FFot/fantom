class HomeCtrl {
  constructor(Tags, AppConstants, $scope,User) {
    'ngInject';

    this.appName = AppConstants.appName;
    this._$scope = $scope;
      this.home={
      description:"" ,
      languages:[],
      owner_first_name:"",
      tags:[],
    
      }

      this.empty=false;
  
    // console.log("printed fromhome ", ArticleListCtrl.list)
    // this.list 

    User.getUsers().then(data=>{
      // console.log()
      this.users=data.users
    })
    // Get list of all tags
    Tags
      .getAllTag()
      .then(
        (tags) => {
          this.tagsLoaded = true;
          this.tags = tags
        }
      );
      Tags
      .getAllLang()
      .then(
        (langs) => {
          this.langsLoaded = true;
          this.langs = langs
        }
      );
     
  }

  allRepositories(){
    this.home={
      description:"" ,
      languages:[],
      owner_first_name:"",
      tags:[],
    
      }

      this.searchDescription(this.home,1)
  }


  changeTag(tag) {
    this.home.tags=[]
    this.home.tags.push(tag)
    console.log(tag)
    this.searchDescription(this.home,0)
  }
  changeLanguage(lang){
    this.home.languages=[]
    this.home.languages.push(lang)
 
    this.searchDescription(this.home,0)
  }

  //have used from 0 for searches, and 1 for reverting the searches and fetching all data
  searchDescription(searchPayload,from){
  if(this.home.description!=""||this.home.languages.length>0||this.home.tags.length>0||this.home.owner_first_name!=""||from==1){
    this._$scope.$broadcast('searchResult', searchPayload);
  }
  else{
    this.empty=true;
  }
    
  
  }
  

}

export default HomeCtrl;
