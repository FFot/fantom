function EditorConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.editor', {
    url: '/editor/:slug',
    controller: 'EditorCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'editor/editor.html',
    title: 'Editor',
    resolve:{
      article: function(Articles, $state, $stateParams) {
        // If we're trying to edit an article
        if ($stateParams.slug) {
          return Articles.get($stateParams.slug).then(
            (article) => {},
            // If there's an error (article doesn't exist, etc), redirect to home page
            (err) => $state.go('app.home')
          );

        // If this is a new article, then just return null
        } else {
          return null;
        }
      }

    }
  });

};

export default EditorConfig;
