class EditorCtrl {
  constructor(Articles, article, $state) {
    'ngInject';

    this._Articles = Articles;
    this._$state = $state;
    this.langField=""
    this.invalid=false;


    if (!article) {
      this.article = {
        description: '',
        tags: [],
        languages:[],
        git_url:"",
        owner_first_name:"",
        owner_last_name:"",
        owner_email:""
        }
      

      
    } else {
      this.article = article;
    }

  }

  addTag() {
    // Make sure this tag isn't already in the array
    if (!this.article.tags.includes(this.tagField)) {
      this.article.tags.push(this.tagField);
      this.tagField = '';
    }
  }

  removeTag(tagName) {
    this.article.tags = this.article.tags.filter((slug) => slug != tagName);
  }
  
  addLang() {
    // Make sure this tag isn't already in the array
    if (!this.article.languages.includes(this.langField)) {
      this.article.languages.push(this.langField);
      this.langField = '';
    }
  }

  removeLang(langName) {
    this.article.languages = this.article.languages.filter((slug) => slug != langName);
  }

  validator(){
    alert("Called")
    var keys=Object.keys(this.article)
for(var key of keys){
  if(typeof this.article[key]=="object"){
    if(this.article[key].length==0){
      this.invalid=true
      break;
    }
    else{
      this.invalid=false
    }
  }else{
  if(this.article[key]==""){
   this.invalid=true
   break;
  }
  else{
    this.invalid=false
  }
}
}
  }

  submit() {
    this.isSubmitting = true;
    this.validator()
    alert(this.invalid)
if(!this.invalid){
    this._Articles.save(this.article).then(
      (newArticle) => {
        console.log(newArticle)
        this._$state.go('app.article', { slug: newArticle.git.description });
      },
      (err) => {
        this.isSubmitting = false;
        this.errors = err.data.errors;
      }
    );
  }
  }
}

export default EditorCtrl;
