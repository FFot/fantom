#!/bin/bash

source variables.txt
export AWS_DEFAULT_PROFILE AWS_PROFILE AWS_DEFAULT_REGION DNS_PREFIX DOMAIN_NAME

cd flask_api
export apiUri=$(aws ecr create-repository --repository-name fantom/api --region $AWS_DEFAULT_REGION | jq -r '.repository.repositoryUri')
aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin $apiUri
docker build -t fantom/api:latest -f Dockerfile.flask .
docker tag fantom/api:latest ${apiUri}:latest
docker push ${apiUri}:latest

cd ../angularjs

echo "const AppConstants = {

  api: 'http://api.${DNS_PREFIX}.${DOMAIN_NAME}',

  appName: 'codejar',
};

export default AppConstants;" >src/js/config/app.constants.js

export wwwUri=$(aws ecr create-repository --repository-name fantom/frontend --region $AWS_DEFAULT_REGION | jq -r '.repository.repositoryUri')
aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin $wwwUri
docker build -t fantom/frontend:latest -f Dockerfile.angularjs .
docker tag fantom/frontend:latest ${wwwUri}:latest
docker push ${wwwUri}:latest

cd ../infra
terraform apply -auto-approve -var api_image="$apiUri" -var www_image="$wwwUri"
