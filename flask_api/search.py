#!/usr/bin/env python3
import mysql.connector
import json
import datetime
import git_functions

def users(args, getcur):
    keys = list(args.keys())
    values = list(args.values())
    if len(keys) == 0:
        return "CANNOT SEARCH: Did not give any search criteria"
    elif len(keys) > 4:
        return "CANNOT SEARCH: There is a maximum of four search criteria"
    else:
        selectStr = "SELECT * FROM users WHERE {} = '{}'".format(keys[0], values[0])
        if len(keys) > 1:
            for i in range(1,len(keys)):
                selectStr += " AND {} = '{}'".format(keys[i], values[i])
        selectStr += ";"
        try:
            getcur.execute(selectStr)
            users = getcur.fetchall()
        except Exception as e:
            return "INVALID SEARCH: "+str(e)
        userlist = []
        for user in users:
            stepDict = {"user_id": user[0], "first_name": user[1], "last_name": user[2], "email": user[3]}
            userlist.append(stepDict)
        outputDict = {"timestamp": datetime.datetime.now().isoformat(), "parameters": args, "query": selectStr, "results": userlist}
        return json.dumps(outputDict)

def tags(args, getcur):
    keys = list(args.keys())
    values = list(args.values())
    if len(keys) == 0:
        return "CANNOT SEARCH: Did not give any search criteria"
    elif len(keys) > 2:
        return "CANNOT SEARCH: There is a maximum of two search criteria"
    else:
        selectStr = "SELECT * FROM tags WHERE {} = '{}'".format(keys[0], values[0])
        if len(keys) > 1:
            for i in range(1,len(keys)):
                selectStr += " AND {} = '{}'".format(keys[i], values[i])
        selectStr += ";"
        try:
            getcur.execute(selectStr)
            tagData = getcur.fetchall()
        except Exception as e:
            return "INVALID SEARCH: "+str(e)
        tagList = []
        for tagEntry in tagData:
            stepDict = {"tag_id": tagEntry[0], "tag": tagEntry[1]}
            tagList.append(stepDict)
        outputDict = {"timestamp": datetime.datetime.now().isoformat(), "parameters": args, "query": selectStr, "results": tagList}
        return json.dumps(outputDict)

def languages(args, getcur):
    keys = list(args.keys())
    values = list(args.values())
    if len(keys) == 0:
        return "CANNOT SEARCH: Did not give any search criteria"
    elif len(keys) > 2:
        return "CANNOT SEARCH: There is a maximum of two search criteria"
    else:
        selectStr = "SELECT * FROM languages WHERE {} = '{}'".format(keys[0], values[0])
        if len(keys) > 1:
            for i in range(1,len(keys)):
                selectStr += " AND {} = '{}'".format(keys[i], values[i])
        selectStr += ";"
        try:
            getcur.execute(selectStr)
            langData = getcur.fetchall()
        except Exception as e:
            return "INVALID SEARCH: "+str(e)
        langList = []
        for langEntry in langData:
            stepDict = {"language_id": langEntry[0], "language_name": langEntry[1]}
            langList.append(stepDict)
        outputDict = {"timestamp": datetime.datetime.now().isoformat(), "parameters": args, "query": selectStr, "results": langList}
        return json.dumps(outputDict)

def gits(req_data, getcur):
    cid_fo = git_functions.code_id_from_owners(req_data, getcur)
    cid_ft = git_functions.code_id_from_tags(req_data, getcur)
    cid_fl = git_functions.code_id_from_languages(req_data, getcur)
    cid_fd = git_functions.code_id_from_description(req_data, getcur)
    code_ids = []
    if cid_fo != None:
        code_ids.append(cid_fo)
    if cid_ft != None:
        code_ids.append(cid_ft)
    if cid_fl != None:
        code_ids.append(cid_fl)
    if cid_fd != None:
        code_ids.append(cid_fd)
    matched_cids = []
    if len(code_ids) > 0:
        for cid in code_ids[0]:
            matched = True
            for line in code_ids:
                if line.count(cid) < 1:
                    matched = False
            if matched:
                matched_cids.append(cid)
    gitList = []
    for cid in matched_cids:
        selectStr = "SELECT * FROM git WHERE code_id = {};".format(cid)
        getcur.execute(selectStr)
        gitEntry = getcur.fetchall()
        stepDict = git_functions.git_listed(gitEntry[0], getcur)
        gitList.append(stepDict)
    outputDict = {"timestamp": datetime.datetime.now().isoformat(), "gits": gitList}
    return json.dumps(outputDict)
