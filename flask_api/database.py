#!/usr/bin/env python3
import mysql.connector
import json
import os

def connect():
    mydb = mysql.connector.connect(
      host=os.environ['DB_HOST'],
      port=os.environ['DB_PORT'],
      user=os.environ['DB_USER'],
      passwd=os.environ['DB_PASSWD'],
      database="toolkits"
    )
    return mydb
