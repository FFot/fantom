#!/bin/bash

repoUri=$(aws ecr create-repository --repository-name fantom/api --region $AWS_DEFAULT_REGION | jq -r '.repository.repositoryUri')
aws ecr get-login-password --region eu-west-2 | docker login --username AWS --password-stdin $repoUri
docker build -t fantom/api:latest -f Dockerfile.flask .
docker tag fantom/api:latest ${repoUri}:latest
docker push ${repoUri}:latest
