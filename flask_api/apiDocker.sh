#!/bin/bash

docker build -t fantomapi:latest -f Dockerfile.flask .
docker run -itd --name FantomAPI -p 5000:5000 --link="FantomDB:pcdb" -e DB_HOST=pcdb fantomapi
