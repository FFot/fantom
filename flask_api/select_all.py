#!/usr/bin/env python3
import mysql.connector
import json
import datetime
import git_functions

def users(mydb):
    getcur = mydb.cursor()
    selectStr = "SELECT * FROM users;"
    getcur.execute(selectStr)
    users = getcur.fetchall()
    getcur.close()
    userlist = []
    for user in users:
        stepDict = {"user_id": user[0], "first_name": user[1], "last_name": user[2], "email": user[3]}
        userlist.append(stepDict)
    outputDict = {"timestamp": datetime.datetime.now().isoformat(), "users": userlist}
    return json.dumps(outputDict)

def languages(mydb):
    getcur = mydb.cursor()
    selectStr = "SELECT * FROM languages;"
    getcur.execute(selectStr)
    languages = getcur.fetchall()
    getcur.close()
    langList = []
    for lang in languages:
        stepDict = {"language_id": lang[0], "language_name": lang[1]}
        langList.append(stepDict)
    outputDict = {"timestamp": datetime.datetime.now().isoformat(), "languages": langList}
    return json.dumps(outputDict)

def tags(mydb):
    getcur = mydb.cursor()
    selectStr = "SELECT * FROM tags;"
    getcur.execute(selectStr)
    tagData = getcur.fetchall()
    getcur.close()
    tagList = []
    for tagEntry in tagData:
        stepDict = {"tag_id": tagEntry[0], "tag": tagEntry[1]}
        tagList.append(stepDict)
    outputDict = {"timestamp": datetime.datetime.now().isoformat(), "tags": tagList}
    return json.dumps(outputDict)

def gits(mydb):
    getcur = mydb.cursor()
    selectStr = "SELECT * FROM git;"
    getcur.execute(selectStr)
    gitData = getcur.fetchall()
    gitList = []
    for gitEntry in gitData:
        stepDict = git_functions.git_listed(gitEntry, getcur)
        gitList.append(stepDict)
    getcur.close()
    outputDict = {"timestamp": datetime.datetime.now().isoformat(), "gits": gitList}
    return json.dumps(outputDict)
