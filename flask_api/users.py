#!/usr/bin/env python3
from flask import Flask, request
import mysql.connector
import json
import datetime;

app = Flask(__name__)

@app.route("/users")
def home():
    mydb = mysql.connector.connect(
      host="127.0.0.1",
      port="32771",
      user="root",
      passwd="secret123",
      database="toolkits"
    )
    getcur = mydb.cursor()
    selectStr = "SELECT * FROM users;"
    getcur.execute(selectStr)
    users = getcur.fetchall()
    getcur.close()
    mydb.close()
    userlist = []
    for user in users:
        stepDict = {"user_id": user[0], "first_name": user[1], "last_name": user[2], "email": user[3]}
        userlist.append(stepDict)
    outputDict = {"timestamp": datetime.datetime.now().isoformat(), "users": userlist}
    return json.dumps(outputDict)

@app.route('/search/users')
def query_example():
    args = dict(request.args)
    keys = list(args.keys())
    values = list(args.values())
    if len(keys) == 0:
        return "CANNOT SEARCH: Did not give any search criteria"
    elif len(keys) > 4:
        return "CANNOT SEARCH: There is a maximum of four search criteria"
    else:
        selectStr = "SELECT * FROM users WHERE {} = '{}'".format(keys[0], values[0])
        if len(keys) > 1:
            for i in range(1,len(keys)):
                selectStr += " AND {} = '{}'".format(keys[i], values[i])
        selectStr += ";"
        mydb = mysql.connector.connect(
          host="127.0.0.1",
          port="32771",
          user="root",
          passwd="secret123",
          database="toolkits"
        )
        getcur = mydb.cursor()
        try:
            getcur.execute(selectStr)
            users = getcur.fetchall()
        except Exception as e:
            return "INVALID SEARCH: "+str(e)
        getcur.close()
        mydb.close()
        userlist = []
        for user in users:
            stepDict = {"user_id": user[0], "first_name": user[1], "last_name": user[2], "email": user[3]}
            userlist.append(stepDict)
        outputDict = {"timestamp": datetime.datetime.now().isoformat(), "parameters": args, "query": selectStr, "results": userlist}
        return json.dumps(outputDict)

@app.route("/add/user", methods=['POST'])
def add_user():
    req_data = request.get_json()
    try:
        fname = req_data['first_name']
        lname = req_data['last_name']
        email = req_data['email']
        mydb = mysql.connector.connect(
          host="127.0.0.1",
          port="32771",
          user="root",
          passwd="secret123",
          database="toolkits"
        )
        getcur = mydb.cursor()
        insertStr = "INSERT INTO users (first_name, last_name, email) VALUES ('{}', '{}', '{}');"
        getcur.execute(insertStr.format(fname, lname, email))
        mydb.commit()
        getcur.close()
        mydb.close()
        return "USER {} {} SUBMITTED".format(fname, lname)
    except:
        return "INVALID DATA: require the following to be assigned: \"first_name\", \"last_name\", \"email\""

if __name__ == "__main__":
    app.run(debug=True)
