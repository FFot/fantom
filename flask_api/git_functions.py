#!/usr/bin/env python3
import mysql.connector
import json
import datetime

def git_listed(gitEntry, getcur):
    ownerStr = "SELECT * FROM users WHERE user_id = "+str(gitEntry[1])+";"
    getcur.execute(ownerStr)
    ownerData = getcur.fetchall()
    langStr = "SELECT cl.language_id, languages.language_name FROM code_languages AS cl INNER JOIN languages WHERE cl.code_id = "+str(gitEntry[0])+" AND languages.language_id = cl.language_id;"
    getcur.execute(langStr)
    langData = getcur.fetchall()
    langList = []
    for langEntry in langData:
        langDict = {"language_id": langEntry[0], "language_name": langEntry[1]}
        langList.append(langDict)
    tagStr = "SELECT ct.tag_id, tags.tag FROM code_tags AS ct INNER JOIN tags WHERE ct.code_id = "+str(gitEntry[0])+" AND tags.tag_id = ct.tag_id;"
    getcur.execute(tagStr)
    tagData = getcur.fetchall()
    tagList = []
    for tagEntry in tagData:
        tagDict = {"tag_id": tagEntry[0], "tag": tagEntry[1]}
        tagList.append(tagDict)
    stepDict = {"code_id": gitEntry[0], "owner": {"user_id": ownerData[0][0], "first_name": ownerData[0][1], "last_name": ownerData[0][2], "email": ownerData[0][3]}, "git_url": gitEntry[2], "description": gitEntry[3], "languages": langList, "tags": tagList}
    return stepDict

def git_full_listed(gitEntry, getcur):
    ownerStr = "SELECT * FROM users WHERE user_id = "+str(gitEntry[1])+";"
    getcur.execute(ownerStr)
    ownerData = getcur.fetchall()
    langStr = "SELECT cl.language_id, languages.language_name FROM code_languages AS cl INNER JOIN languages WHERE cl.code_id = "+str(gitEntry[0])+" AND languages.language_id = cl.language_id;"
    getcur.execute(langStr)
    langData = getcur.fetchall()
    langList = []
    for langEntry in langData:
        langDict = {"language_id": langEntry[0], "language_name": langEntry[1]}
        langList.append(langDict)
    tagStr = "SELECT ct.tag_id, tags.tag FROM code_tags AS ct INNER JOIN tags WHERE ct.code_id = "+str(gitEntry[0])+" AND tags.tag_id = ct.tag_id;"
    getcur.execute(tagStr)
    tagData = getcur.fetchall()
    tagList = []
    for tagEntry in tagData:
        tagDict = {"tag_id": tagEntry[0], "tag": tagEntry[1]}
        tagList.append(tagDict)
    contStr = "SELECT ct.contributor_id, users.first_name, users.last_name, users.email FROM contributors AS ct INNER JOIN users WHERE ct.code_id = "+str(gitEntry[0])+" AND users.user_id = ct.contributor_id;"
    getcur.execute(contStr)
    contData = getcur.fetchall()
    contList = []
    for contEntry in contData:
        contDict = {"user_id": contEntry[0], "first_name": contEntry[1], "last_name": contEntry[2], "email": contEntry[3]}
        contList.append(contDict)
    stepDict = {"code_id": gitEntry[0], "owner": {"user_id": ownerData[0][0], "first_name": ownerData[0][1], "last_name": ownerData[0][2], "email": ownerData[0][3]}, "git_url": gitEntry[2], "description": gitEntry[3], "languages": langList, "tags": tagList, "contributors": contList}
    return stepDict

def code_id_from_owners(req_data, getcur):
    try:
        owner_id = [req_data['owner_id']]
    except:
        try:
            owner_first_name = req_data['owner_first_name']
        except:
            owner_first_name = None
        try:
            owner_last_name = req_data['owner_last_name']
        except:
            owner_last_name = None
        if owner_first_name and owner_last_name:
            selectStr = "SELECT user_id FROM users WHERE first_name = '{}' AND last_name = '{}';".format(owner_first_name, owner_last_name)
            try:
                getcur.execute(selectStr)
                results = getcur.fetchall()
                owner_id = []
                for res in results:
                    owner_id.append(res[0])
            except Exception as e:
                owner_id = None
        elif owner_last_name:
            selectStr = "SELECT user_id FROM users WHERE last_name = '{}';".format(owner_last_name)
            try:
                getcur.execute(selectStr)
                results = getcur.fetchall()
                owner_id = []
                for res in results:
                    owner_id.append(res[0])
            except Exception as e:
                owner_id = None
        elif owner_first_name:
            selectStr = "SELECT user_id FROM users WHERE first_name = '{}';".format(owner_first_name)
            try:
                getcur.execute(selectStr)
                results = getcur.fetchall()
                owner_id = []
                for res in results:
                    owner_id.append(res[0])
            except Exception as e:
                owner_id = None
        else:
            owner_id = None
    if owner_id and len(owner_id) > 0:
        selectStr = "SELECT code_id FROM git WHERE owner_id = {}".format(owner_id[0])
        if len(owner_id) > 1:
            for i in range(1,len(owner_id)):
                selectStr += " OR owner_id = {}".format(owner_id[i])
        selectStr += ";"
        try:
            getcur.execute(selectStr)
            results = getcur.fetchall()
            code_id = []
            for res in results:
                code_id.append(res[0])
            return code_id
        except Exception as e:
            return None
    elif owner_id:
        return []
    else:
        return None

def code_id_from_tags(req_data, getcur):
    try:
        tagList = req_data['tags']
        tagIds = []
        for tagEntry in tagList:
            if isinstance(tagEntry, int):
                tagIds.append(tagEntry)
            elif isinstance(tagEntry, str):
                selectStr = "SELECT tag_id FROM tags WHERE tag = '{}';".format(tagEntry)
                try:
                    getcur.execute(selectStr)
                    tagData = getcur.fetchall()
                    tagIds.append(tagData[0][0])
                except Exception as e:
                    pass
    except:
        tagIds = None
    if tagIds and len(tagIds) > 0:
        selectStr = "SELECT code_id FROM code_tags WHERE tag_id = {}".format(tagIds[0])
        if len(tagIds) > 1:
            for i in range(1,len(tagIds)):
                selectStr += " OR tag_id = {}".format(tagIds[i])
        selectStr += ";"
        try:
            getcur.execute(selectStr)
            results = getcur.fetchall()
            code_id = []
            for res in results:
                code_id.append(res[0])
            return code_id
        except Exception as e:
            return None
    else:
        None

def code_id_from_languages(req_data, getcur):
    try:
        langList = req_data['languages']
        langIds = []
        for langEntry in langList:
            if isinstance(langEntry, int):
                langIds.append(langEntry)
            elif isinstance(langEntry, str):
                selectStr = "SELECT language_id FROM languages WHERE language_name = '{}';".format(langEntry)
                try:
                    getcur.execute(selectStr)
                    langData = getcur.fetchall()
                    langIds.append(langData[0][0])
                except Exception as e:
                    pass
    except:
        langIds = None
    if langIds and len(langIds) > 0:
        selectStr = "SELECT code_id FROM code_languages WHERE language_id = {}".format(langIds[0])
        if len(langIds) > 1:
            for i in range(1,len(langIds)):
                selectStr += " OR language_id = {}".format(langIds[i])
        selectStr += ";"
        try:
            getcur.execute(selectStr)
            results = getcur.fetchall()
            code_id = []
            for res in results:
                code_id.append(res[0])
            return code_id
        except Exception as e:
            return None
    else:
        None

def code_id_from_description(req_data, getcur):
    try:
        desString = req_data['description']
        selectStr = "SELECT code_id FROM git WHERE description LIKE '%{}%';".format(desString)
        getcur.execute(selectStr)
        desData = getcur.fetchall()
        code_ids = []
        for desEntry in desData:
            code_ids.append(desEntry[0])
        return code_ids
    except:
        return None

def get_language_ids(req_data, getcur, mydb):
    try:
        langList = req_data['languages']
        langIds = []
        for langEntry in langList:
            if isinstance(langEntry, int):
                selectStr = "SELECT language_id FROM languages WHERE language_id = {};".format(langEntry)
                try:
                    getcur.execute(selectStr)
                    langData = getcur.fetchall()
                    if len(langData) > 0:
                        langIds.append(langEntry)
                except:
                    pass
            elif isinstance(langEntry, str):
                selectStr = "SELECT language_id FROM languages WHERE language_name = '{}';".format(langEntry)
                try:
                    getcur.execute(selectStr)
                    langData = getcur.fetchall()
                    if len(langData) > 0:
                        langIds.append(langData[0][0])
                    else:
                        raise Exception("Couldn't find language")
                except:
                    insertStr = "INSERT IGNORE INTO languages (language_name) VALUES ('{}');"
                    getcur.execute(insertStr.format(langEntry))
                    mydb.commit()
                    selectStr = "SELECT language_id FROM languages WHERE language_name = '{}';".format(langEntry)
                    try:
                        getcur.execute(selectStr)
                        langData = getcur.fetchall()
                        if len(langData) > 0:
                            langIds.append(langData[0][0])
                    except:
                        pass
    except:
        langIds = None
    return langIds

def get_tag_ids(req_data, getcur, mydb):
    try:
        tagList = req_data['tags']
        tagIds = []
        for tagEntry in tagList:
            if isinstance(tagEntry, int):
                selectStr = "SELECT tag_id FROM tags WHERE tag_id = {};".format(tagEntry)
                try:
                    getcur.execute(selectStr)
                    tagData = getcur.fetchall()
                    if len(tagData) > 0:
                        tagIds.append(tagEntry)
                except:
                    pass
            elif isinstance(tagEntry, str):
                selectStr = "SELECT tag_id FROM tags WHERE tag = '{}';".format(tagEntry)
                try:
                    getcur.execute(selectStr)
                    tagData = getcur.fetchall()
                    if len(tagData) > 0:
                        tagIds.append(tagData[0][0])
                    else:
                        raise Exception("Couldn't find tag")
                except:
                    insertStr = "INSERT IGNORE INTO tags (tag) VALUES ('{}');"
                    getcur.execute(insertStr.format(tagEntry))
                    mydb.commit()
                    selectStr = "SELECT tag_id FROM tags WHERE tag = '{}';".format(tagEntry)
                    try:
                        getcur.execute(selectStr)
                        tagData = getcur.fetchall()
                        if len(tagData) > 0:
                            tagIds.append(tagData[0][0])
                    except:
                        pass
    except:
        tagIds = None
    return tagIds

def add_git(owner_id,req_data,getcur,mydb):
    try:
        description = req_data['description']
        selectStr = "SELECT code_id FROM git WHERE description = '{}';".format(description)
        try:
            getcur.execute(selectStr)
            desData = getcur.fetchall()
        except:
            pass
        if desData and len(desData) > 0:
            raise Exception("Found git with that description")
    except:
        return "INVALID: Description is identical to a git that exists already"
    try:
        git_url = req_data['git_url']
    except:
        return "INVALID: Did not supply anything under \"git_url\" (it accepts empty field \"\")"
    try:
        selectStr = "SELECT code_id FROM git WHERE git_url = '{}';".format(git_url)
        try:
            getcur.execute(selectStr)
            desData = getcur.fetchall()
        except:
            pass
        if desData and len(desData) > 0:
            raise Exception("Found git with that URL")
    except:
        return "INVALID: URL is identical to a git that exists already"
    insertStr = "INSERT IGNORE INTO git (owner_id, git_url, description) VALUES ({}, '{}', '{}');"
    getcur.execute(insertStr.format(owner_id, git_url, description))
    mydb.commit()
    selectStr = "SELECT code_id FROM git WHERE owner_id = {} AND git_url = '{}' AND  description = '{}';"
    try:
        getcur.execute(selectStr.format(owner_id, git_url, description))
        gitData = getcur.fetchall()
        if len(gitData) == 1:
            gitID = gitData[0][0]
        else:
            raise Exception("Couldn't find git")
    except:
        return "FAILURE: Couldn't get code ID after attempting creation"
    langIds = get_language_ids(req_data, getcur, mydb)
    if langIds != None:
        for id in langIds:
            try:
                insertStr = "INSERT IGNORE INTO code_languages (code_id, language_id) VALUES ({}, {});"
                getcur.execute(insertStr.format(gitID, id))
                mydb.commit()
            except:
                pass
    tagIds = get_tag_ids(req_data, getcur, mydb)
    if tagIds != None:
        for id in tagIds:
            try:
                insertStr = "INSERT IGNORE INTO code_tags (code_id, tag_id) VALUES ({}, {});"
                getcur.execute(insertStr.format(gitID, id))
                mydb.commit()
            except:
                pass
    selectStr = "SELECT * FROM git WHERE code_id = {};"
    getcur.execute(selectStr.format(gitID))
    gitEntry = getcur.fetchone()
    gitInfo = git_listed(gitEntry, getcur)
    outputDict = {"timestamp": datetime.datetime.now().isoformat(), "git": gitInfo}
    return json.dumps(outputDict)

def add_git_existing_user(req_data,mydb):
    getcur = mydb.cursor()
    try:
        owner_id = req_data['owner_id']
        selectStr = "SELECT user_id FROM users WHERE user_id = {};".format(owner_id)
        try:
            getcur.execute(selectStr)
            userData = getcur.fetchall()
            if len(userData) == 0:
                raise Exception("Couldn't find user")
        except:
            raise Exception("Couldn't find user")
    except:
        return "INVALID: User does not exist"
    output = add_git(owner_id,req_data,getcur,mydb)
    getcur.close()
    return output

def add_git_new_user(req_data,mydb):
    getcur = mydb.cursor()
    owner_fname = req_data['owner_first_name']
    owner_lname = req_data['owner_last_name']
    owner_email = req_data['owner_email']
    selectStr = "SELECT user_id FROM users WHERE first_name = '{}' AND last_name = '{}' AND email = '{}';".format(owner_fname, owner_lname, owner_email)
    try:
        getcur.execute(selectStr)
        userData = getcur.fetchall()
        if len(userData) > 0:
            owner_id = userData[0][0]
        else:
            raise Exception("Couldn't find user")
    except:
        insertStr = "INSERT IGNORE INTO users (first_name, last_name, email) VALUES ('{}', '{}', '{}');"
        try:
            getcur.execute(insertStr.format(owner_fname, owner_lname, owner_email))
            mydb.commit()
        except:
            return "FAILURE: Failed to add user"
        selectStr = "SELECT user_id FROM users WHERE first_name = '{}' AND last_name = '{}' AND email = '{}';".format(owner_fname, owner_lname, owner_email)
        try:
            getcur.execute(selectStr)
            userData = getcur.fetchall()
            if len(userData) > 0:
                owner_id = userData[0][0]
            else:
                raise Exception("Couldn't find user")
        except:
            return "FAILURE: Couldn't find ID after attempting to create user"
    output = add_git(owner_id,req_data,getcur,mydb)
    getcur.close()
    return output
