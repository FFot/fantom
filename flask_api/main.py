from flask import Flask, request
from flask_cors import CORS, cross_origin
from waitress import serve
import mysql.connector
import json
import datetime
import select_all
import search
import git_functions
import database
from waitress import serve

app = Flask(__name__)
#CORS(app, resources={ r'/*': {'origins': ['codejar.academy.grads.al-labs.co.uk']}}, supports_credentials=True)
#CORS(app)
#app.config['CORS_HEADERS'] = 'Content-Type'

@app.after_request
def after_request(response):
#    del response.headers["Access-Control-Allow-Origin"]
#    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Headers", "*")
    response.headers.add("Access-Control-Allow-Methods", "*")
    return response

@app.route("/")
def intro():
    return "Welcome to the Fantom API!"

@app.route("/users")
@cross_origin()
def all_users():
    mydb = database.connect()
    jsonData = select_all.users(mydb)
    mydb.close()
    return jsonData

@app.route("/languages")
@cross_origin()
def all_languages():
    mydb = database.connect()
    jsonData = select_all.languages(mydb)
    mydb.close()
    return jsonData

@app.route("/tags")
@cross_origin()
def all_tags():
    mydb = database.connect()
    jsonData = select_all.tags(mydb)
    mydb.close()
    return jsonData

@app.route("/gits")
@cross_origin()
def all_gits():
    mydb = database.connect()
    jsonData = select_all.gits(mydb)
    mydb.close()
    return jsonData

@app.route('/search/users')
@cross_origin()
def search_user():
    mydb = database.connect()
    getcur = mydb.cursor()
    args = dict(request.args)
    output = search.users(args, getcur)
    getcur.close()
    mydb.close()
    return output

@app.route('/search/tags')
@cross_origin()
def search_tag():
    mydb = database.connect()
    getcur = mydb.cursor()
    args = dict(request.args)
    output = search.tags(args, getcur)
    getcur.close()
    mydb.close()
    return output

@app.route('/search/languages')
@cross_origin()
def search_language():
    mydb = database.connect()
    getcur = mydb.cursor()
    args = dict(request.args)
    output = search.languages(args, getcur)
    getcur.close()
    mydb.close()
    return output

@app.route('/search/gits', methods=['POST'])
@cross_origin()
def search_git():
    mydb = database.connect()
    getcur = mydb.cursor()
    req_data = request.get_json()
    output = search.gits(req_data, getcur)
    getcur.close()
    mydb.close()
    return output

@app.route("/add/user", methods=['POST'])
@cross_origin()
def add_user():
    req_data = request.get_json()
    try:
        fname = req_data['first_name']
        lname = req_data['last_name']
        email = req_data['email']
        mydb = database.connect()
        getcur = mydb.cursor()
        selectStr = "SELECT * FROM users WHERE first_name = '{}' AND last_name = '{}' AND email = '{}';"
        getcur.execute(selectStr.format(fname, lname, email))
        testUsers = getcur.fetchall()
        if len(testUsers) > 0:
            return "USER ALREADY SUBMITTED"
        insertStr = "INSERT IGNORE INTO users (first_name, last_name, email) VALUES ('{}', '{}', '{}');"
        getcur.execute(insertStr.format(fname, lname, email))
        mydb.commit()
        getcur.close()
        mydb.close()
        return "USER {} {} SUBMITTED".format(fname, lname)
    except:
        return "INVALID DATA: require the following to be assigned: \"first_name\", \"last_name\", \"email\""

@app.route("/add/tag", methods=['POST'])
@cross_origin()
def add_tag():
    req_data = request.get_json()
    try:
        tname = req_data['tag']
        mydb = database.connect()
        getcur = mydb.cursor()
        selectStr = "SELECT * FROM tags WHERE tag = '{}';"
        getcur.execute(selectStr.format(tname))
        testTags = getcur.fetchall()
        if len(testTags) > 0:
            return "TAG ALREADY SUBMITTED"
        insertStr = "INSERT IGNORE INTO tags (tag) VALUES ('{}');"
        getcur.execute(insertStr.format(tname))
        mydb.commit()
        getcur.close()
        mydb.close()
        return "TAG {} SUBMITTED".format(tname)
    except:
        return "INVALID DATA: require the following to be assigned: \"tag\""

@app.route("/add/language", methods=['POST'])
@cross_origin()
def add_language():
    req_data = request.get_json()
    try:
        lname = req_data['language_name']
        mydb = database.connect()
        getcur = mydb.cursor()
        selectStr = "SELECT * FROM languages WHERE language_name = '{}';"
        getcur.execute(selectStr.format(lname))
        testLangs = getcur.fetchall()
        if len(testLangs) > 0:
            return "LANGUAGE ALREADY SUBMITTED"
        insertStr = "INSERT IGNORE INTO languages (language_name) VALUES ('{}');"
        getcur.execute(insertStr.format(lname))
        mydb.commit()
        getcur.close()
        mydb.close()
        return "LANGUAGE {} SUBMITTED".format(lname)
    except:
        return "INVALID DATA: require the following to be assigned: \"language_name\""

@app.route("/add/git", methods=['POST'])
@cross_origin()
def add_git():
    req_data = request.get_json()
    mydb = database.connect()
    try:
        descript = req_data['description']
    except:
        mydb.close()
        return "INVALID: Need to supply a description for git"
    try:
        owner_id = req_data['owner_id']
        output = git_functions.add_git_existing_user(req_data,mydb)
    except:
        try:
            owner_fname = req_data['owner_first_name']
            owner_lname = req_data['owner_last_name']
            owner_email = req_data['owner_email']
        except:
            mydb.close()
            return "INVALID: Require ID of existing user or full details of new user"
        output = git_functions.add_git_new_user(req_data,mydb)
    mydb.close()
    return output

@app.route('/git/<git_id>')
@cross_origin()
def show_git(git_id):
    mydb = database.connect()
    getcur = mydb.cursor()
    selectStr = "SELECT * FROM git WHERE code_id = "+str(git_id)+";"
    getcur.execute(selectStr)
    gitData = getcur.fetchone()
    if gitData:
        fullGit = git_functions.git_full_listed(gitData, getcur)
        outputDict = {"timestamp": datetime.datetime.now().isoformat(), "git": fullGit}
        jsonData = json.dumps(outputDict)
    else:
        jsonData = "No such git"
    getcur.close()
    mydb.close()
    return jsonData

@app.route('/addtogit/<git_id>/contributor')
@cross_origin()
def add_contributor_to_git(git_id):
    conID = request.args['id']
    mydb = database.connect()
    getcur = mydb.cursor()
    selectStr = "SELECT * FROM contributors WHERE code_id = {} AND contributor_id = {};"
    getcur.execute(selectStr.format(git_id,conID))
    test = getcur.fetchall()
    if len(test) > 0:
        return "Contributor already added to git"
    insertStr = "INSERT IGNORE INTO contributors (code_id, contributor_id) VALUES ({}, {});"
    getcur.execute(insertStr.format(git_id,conID))
    mydb.commit()
    getcur.close()
    mydb.close()
    return "Contributor added to git"

@app.route('/addtogit/<git_id>/tag')
@cross_origin()
def add_tag_to_git(git_id):
    conID = request.args['id']
    mydb = database.connect()
    getcur = mydb.cursor()
    selectStr = "SELECT * FROM code_tags WHERE code_id = {} AND tag_id = {};"
    getcur.execute(selectStr.format(git_id,conID))
    test = getcur.fetchall()
    if len(test) > 0:
        return "Tag already added to git"
    insertStr = "INSERT IGNORE INTO code_tags (code_id, tag_id) VALUES ({}, {});"
    getcur.execute(insertStr.format(git_id,conID))
    mydb.commit()
    getcur.close()
    mydb.close()
    return "Tag added to git"

@app.route('/addtogit/<git_id>/language')
@cross_origin()
def add_language_to_git(git_id):
    conID = request.args['id']
    mydb = database.connect()
    getcur = mydb.cursor()
    selectStr = "SELECT * FROM code_languages WHERE code_id = {} AND language_id = {};"
    getcur.execute(selectStr.format(git_id,conID))
    test = getcur.fetchall()
    if len(test) > 0:
        return "Language already added to git"
    insertStr = "INSERT IGNORE INTO code_languages (code_id, language_id) VALUES ({}, {});"
    getcur.execute(insertStr.format(git_id,conID))
    mydb.commit()
    getcur.close()
    mydb.close()
    return "Language added to git"

if __name__ == "__main__":
    #app.run(debug=True,host='0.0.0.0')
    serve(app, host='0.0.0.0', port=5000)
