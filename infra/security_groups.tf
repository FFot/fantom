#-----------Create Security Groups---------

#Public Security group

resource "aws_security_group" "public_sg" {
  name        = "PublicSecurityGroup"
  description = "Used for public and private instances for load balancer access"
  vpc_id      = aws_vpc.vpc.id

  #HTTP

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #Outbound internet access

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Private Security Group

resource "aws_security_group" "private_sg" {
  name        = "PrivateSecurityGroup"
  description = "Used for private instances"
  vpc_id      = aws_vpc.vpc.id

  # Access from other security groups

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.cidr_blocks]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#RDS Security Group

resource "aws_security_group" "rds_sg" {
  name        = "RDSSecurityGroup"
  description = "Used for DB instances"
  vpc_id      = aws_vpc.vpc.id

  # SQL access from public/private security group

  ingress {
    from_port = 3306
    to_port   = 3306
    protocol  = "tcp"

    security_groups = [
      aws_security_group.public_sg.id,
      aws_security_group.private_sg.id,
      aws_security_group.ecs_api.id,
      aws_security_group.ecs_www.id
    ]
  }
}

# Sec grps for ECS cluster
# Traffic to the ECS cluster should only come from the ALB
resource "aws_security_group" "ecs_api" {
  name        = "ecs-api-security-group"
  description = "allow inbound access from the ALB only"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    protocol        = "tcp"
    from_port       = var.api_port
    to_port         = var.api_port
    security_groups = [aws_security_group.public_sg.id,aws_security_group.private_sg.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ecs_www" {
  name        = "ecs-www-security-group"
  description = "allow inbound access from the ALB only"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    protocol        = "tcp"
    from_port       = var.www_port
    to_port         = var.www_port
    security_groups = [aws_security_group.public_sg.id,aws_security_group.private_sg.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
