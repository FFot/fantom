##--------------------Define storing files---------------------##
locals {
  public_key_filename = format(
    "%s/%s%s",
    data.external.pwd.result.dir,
    var.key,
    var.public_key_extension
  )

  private_key_filename = format(
    "%s/%s%s",
    data.external.pwd.result.dir,
    var.key,
    var.private_key_extension
  )
}

##-------------Create SSH key if it doesn't exist-------------##
resource "tls_private_key" "default" {
  algorithm = var.ssh_key_algorithm
}

resource "aws_key_pair" "generated" {
  depends_on = [tls_private_key.default]
  key_name   = var.key
  public_key = tls_private_key.default.public_key_openssh
}

##-----------------Store key details locally------------------##
resource "local_file" "public_key_openssh" {
  depends_on = [tls_private_key.default]
  content    = tls_private_key.default.public_key_openssh
  filename   = local.public_key_filename
}

resource "local_file" "private_key_pem" {
  depends_on        = [tls_private_key.default]
  sensitive_content = tls_private_key.default.private_key_pem
  filename          = local.private_key_filename
  file_permission   = "0600"
}

#---------Add key pair key to bucket------
resource "aws_s3_bucket_object" "key" {
  depends_on = [local_file.private_key_pem]
  bucket     = var.s3_bucket
  key        = "${var.key}.pem"
  source     = "${data.external.pwd.result.dir}/${var.key}"

  #etag = filemd5("${data.external.pwd.result.dir}/${var.key}"
}
