resource "aws_ecs_cluster" "www" {
  name = "www-cluster"
}

resource "aws_ecs_task_definition" "www" {
  family                   = "www"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "${var.fargate_cpu}"
  memory                   = "${var.fargate_memory}"
  execution_role_arn       = aws_iam_role.ecs.arn
  task_role_arn            = aws_iam_role.ecs.arn

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.fargate_cpu},
    "image": "${var.www_image}",
    "memory": ${var.fargate_memory},
    "name": "www",
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": ${var.www_port},
        "hostPort": ${var.www_port}
      }
    ]
  }
]
DEFINITION
}

resource "aws_ecs_service" "www" {
  name            = "www-ecs-service"
  cluster         = "${aws_ecs_cluster.www.id}"
  task_definition = "${aws_ecs_task_definition.www.arn}"
  desired_count   = "${var.www_count}"
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = ["${aws_security_group.ecs_www.id}"]
    subnets          = ["${aws_subnet.public_subnet_1.id}", "${aws_subnet.public_subnet_2.id}"]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.www.id}"
    container_name   = "www"
    container_port   = "${var.www_port}"
  }

  depends_on = [
    "aws_alb_listener.www",
    "aws_iam_role_policy_attachment.ecs"
  ]
}
