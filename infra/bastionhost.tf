#----------Copy schema to s3-------
resource "aws_s3_bucket_object" "schema" {
  bucket = var.s3_bucket
  key    = "schema.sql"
  source = "../database/schema.sql"
}

#----------Create Bastion--------
resource "aws_instance" "bastion" {
  ami                    = var.ami_bastion
  instance_type          = var.instance_type_a
  key_name               = var.key
  subnet_id              = aws_subnet.public_subnet_1.id
  iam_instance_profile   = aws_iam_instance_profile.s3_access_profile.id
  vpc_security_group_ids = ["${aws_security_group.private_sg.id}"]
  user_data              = <<-EOF
                             #!/bin/bash -xv
                             yum install -y mysql56-server
                             yum install -y mysql
                             aws s3 cp s3://${var.s3_bucket}/schema.sql /tmp/schema.sql
                             mysql -h ${module.db.this_db_instance_address} -P 3306 -u ${var.dbusername} -p${var.dbpassword} </tmp/schema.sql
                           EOF
  tags                   = var.tags-bastion
  depends_on             = [aws_s3_bucket_object.schema, aws_s3_bucket_object.key]
}

output "bastion_public_dns" {
  value = aws_instance.bastion.public_dns
}

resource "aws_eip" "bastion_eip" {
  vpc = true
}

# Associate EIP to Bastion instance
resource "aws_eip_association" "bastion_eip_assoc" {
  depends_on    = [aws_internet_gateway.internet_gateway]
  instance_id   = aws_instance.bastion.id
  allocation_id = aws_eip.bastion_eip.id
}
