#--------provider-------
variable "aws_region" {
  type = string
}
variable "aws_profile" {
  type = string
}

#--------VPC--------
data "aws_availability_zones" "available" {}
variable "vpc_cidr" {}
variable "cidrs" {
  type = map
}
variable "cidr_blocks" {}

variable "zone_id" {}

#--------DNS-------------
variable "domain_name" {
  type = string
}

variable "dns_prefix" {
  type = string
}

#------Instances---------

variable "instance_type_a" {
  type = string
}
variable "key" {
  type = string
}
variable "ami" {
  type = string
}
variable "ami_bastion" {
  type = string
}

#--------Tags-------------
variable "project-tag" {
  type = string
}

variable "tags-db" {
  type = map(string)
  default = {
    Name = "database"
  }
}

variable "tags-bastion" {
  type = map(string)
  default = {
    Name = "bastion"
  }
}

#-------S3 Bucket--------
variable "s3_bucket" {
  type = string
}

#---------RDS-------------
variable "snapshotid" {
  type = string
}

variable "instance_class" {
  type = string
}

variable "publicly_accessible" {
  type = bool
}

variable "engine" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "storage_type" {
  type = string
}

variable "allocated_storage" {
  type = string
}

variable "backup_retention_period" {
  type = string
}

variable "backup_window" {
  type = string
}

variable "maintenance_window" {
  type = string
}

variable "maj_eng_ver" {
  type = string
}

variable "parameter_group_name" {
  type = string
}

variable "skip_final_snapshot" {
  type = bool
}

variable "db_subnet_group_name" {
  type = string
}

##------DB login details--------##
variable "dbname" {
  type = string
}

variable "dbusername" {
  type = string
}

variable "dbpassword" {
  type = string
}

##------firewall rules for secgrp------##
variable "port_mysql" {
  type    = string
  default = "3306"
}

variable "port_ssh" {
  type    = string
  default = "22"
}

#---------ELB-------------
variable "elb_healthy_threshold" {}
variable "elb_unhealthy_threshold" {}
variable "elb_timeout" {}
variable "elb_interval" {}

#---------ASG-------------
variable "asg_max" {}
variable "asg_min" {}
variable "asg_grace" {}
variable "asg_hct" {}
variable "asg_cap" {}

#--------ecs--------------
variable "ecs_task_execution_role_name" {}
variable "ecs_auto_scale_role_name" {}
variable "az_count" {}
variable "api_image" {}
variable "api_port" {}
variable "api_count" {}
variable "www_image" {}
variable "www_port" {}
variable "www_count" {}
variable "health_check_path" {
  default = "/"
}
variable "fargate_cpu" {}
variable "fargate_memory" {}

#--------ssh---------

variable "ssh_key_algorithm" {
  type    = string
  default = "RSA"
}

variable "private_key_extension" {
  type    = string
  default = ""
}

variable "public_key_extension" {
  type    = string
  default = ".pub"
}
