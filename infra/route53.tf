resource "aws_route53_record" "api" {
  zone_id = var.zone_id
  name    = "api.${var.dns_prefix}.${var.domain_name}"
  type    = "A"

  alias {
    name                   = aws_alb.api.dns_name
    zone_id                = aws_alb.api.zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "www" {
  zone_id = var.zone_id
  name    = "www.${var.dns_prefix}.${var.domain_name}"
  type    = "A"

  alias {
    name                   = aws_alb.www.dns_name
    zone_id                = aws_alb.www.zone_id
    evaluate_target_health = false
  }
}
