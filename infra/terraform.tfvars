#-----provider-----
aws_profile = "academy2"
aws_region  = "eu-west-2"

#-------VPC--------
vpc_cidr = "10.0.0.0/16"
cidrs = {
  public1  = "10.0.1.0/24"
  public2  = "10.0.2.0/24"
  private1 = "10.0.3.0/24"
  private2 = "10.0.4.0/24"
  rds1     = "10.0.5.0/24"
  rds2     = "10.0.6.0/24"
  rds3     = "10.0.7.0/24"
}

cidr_blocks = ["10.0.0.0/16"]

zone_id = "Z07626429N74Z31VDFLI"

#------SSH Key---------
generate_ssh_key = true #If set to true, new SSH key is generated

#--------DNS-------------
domain_name = "academy.grads.al-labs.co.uk"
dns_prefix  = "codejar"

#--------Instance Default-------------
instance_type_j = "t3.large"
instance_type_a = "t2.micro"
ami             = "ami-01a6e31ac994bbc09"

#-------Instance Bastion--------
ami_bastion = "ami-01a6e31ac994bbc09"

#-------S3 Bucket--------
s3_bucket = "fantom-s3-bucket"

#---------ELB-------------
elb_healthy_threshold   = "2"
elb_unhealthy_threshold = "2"
elb_timeout             = "3"
elb_interval            = "30"

#---------ASG-------------
asg_max   = "2"
asg_min   = "1"
asg_grace = "300"
asg_hct   = "EC2"
asg_cap   = "2"

#---------Tag-------------
project-tag = "fantom"

#----------DB---------------
dbname                  = "fantomdb"
dbusername              = "root"
snapshotid              = null
instance_class          = "db.t2.micro"
publicly_accessible     = true
engine                  = "mariadb"
engine_version          = "10.2.21"
storage_type            = "gp2"
allocated_storage       = 5
backup_retention_period = 30
backup_window           = "23:30-00:30"
maintenance_window      = "Sat:02:00-Sat:05:00"
maj_eng_ver             = 10.2
parameter_group_name    = "default.mariadb10.2"
skip_final_snapshot     = false
db_subnet_group_name    = "rds_subnetgroup"

#--------ECS-----------
ecs_task_execution_role_name = "myEcsTaskExecutionRole"
ecs_auto_scale_role_name     = "myEcsAutoScaleRole"
az_count                     = "2"
api_port                     = 5000
api_count                    = 1
www_port                     = 80
www_count                    = 1
fargate_cpu                  = "1024"
fargate_memory               = "2048"

#--------ssh----------
key = "fantomKey"
