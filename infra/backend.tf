# Add tfstate to bucket
terraform {
  backend "s3" {
    bucket = "fantom-s3-bucket"
    key    = "terraform.tfstate"
    region = "eu-west-2"
  }
}
