resource "aws_ecs_cluster" "api" {
  name = "api-cluster"
}

resource "aws_ecs_task_definition" "api" {
  family                   = "api"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "${var.fargate_cpu}"
  memory                   = "${var.fargate_memory}"
  execution_role_arn       = aws_iam_role.ecs.arn
  task_role_arn            = aws_iam_role.ecs.arn

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.fargate_cpu},
    "image": "${var.api_image}",
    "memory": ${var.fargate_memory},
    "name": "api",
    "networkMode": "awsvpc",
    "environment": [
      {
        "name": "DB_HOST",
        "value": "${module.db.this_db_instance_address}"
      },
      {
        "name": "DB_PORT",
        "value": "${var.port_mysql}"
      },
      {
        "name": "DB_USER",
        "value": "${var.dbusername}"
      },
      {
        "name": "DB_PASSWD",
        "value": "${var.dbpassword}"
      }
    ],
    "portMappings": [
      {
        "containerPort": ${var.api_port},
        "hostPort": ${var.api_port}
      }
    ]
  }
]
DEFINITION
}

resource "aws_ecs_service" "api" {
  name            = "api-ecs-service"
  cluster         = "${aws_ecs_cluster.api.id}"
  task_definition = "${aws_ecs_task_definition.api.arn}"
  desired_count   = "${var.api_count}"
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = ["${aws_security_group.ecs_api.id}"]
    subnets          = ["${aws_subnet.public_subnet_1.id}", "${aws_subnet.public_subnet_2.id}"]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.api.id}"
    container_name   = "api"
    container_port   = "${var.api_port}"
  }

  depends_on = [
    "aws_alb_listener.api",
    "aws_iam_role_policy_attachment.ecs"
  ]
}
