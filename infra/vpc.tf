#-------------Create VPC-------------

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "VPC"
  }
}

#-------------Create internet gateway-------------

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "InternetGateway"
  }
}

#-------------Create Route tables-------------

#Public Route Table
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  tags = {
    Name = "PublicRouteTable"
  }
}

# Private Route Table
resource "aws_default_route_table" "private_rt" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id

  tags = {
    Name = "PrivateRouteTable"
  }
}

#-------------Create Subnets---------------

#Create Public Subnet 1

resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["public1"]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "PublicSubnet1"
  }
}

#Create Public Subnet 2

resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["public2"]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "PublicSubnet2"
  }
}

#Create Private Subnet 1

resource "aws_subnet" "private_subnet_1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["private1"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "PrivateSubnetApp1"
  }
}

#Create Private Subnet 2

resource "aws_subnet" "private_subnet_2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["private2"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "PrivateSubnetApp2"
  }
}

#RDS Private Subnet 1

resource "aws_subnet" "rds_subnet_1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["rds1"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "RDSSubnet1"
  }
}

#RDS Private Subnet 2

resource "aws_subnet" "rds_subnet_2" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["rds2"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "RDSSubnet2"
  }
}

#RDS Private Subnet 3

resource "aws_subnet" "rds_subnet_3" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidrs["rds3"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[2]

  tags = {
    Name = "RDSSubnet3"
  }
}


#-----Create VPC Endpoint for S3 bucket------

resource "aws_vpc_endpoint" "private_endpoint" {
  vpc_id       = aws_vpc.vpc.id
  service_name = "com.amazonaws.${var.aws_region}.s3"

  route_table_ids = [aws_vpc.vpc.main_route_table_id,
    aws_route_table.public_rt.id,
  ]

  policy = <<POLICY
{
    "Statement": [
        {
            "Action": "*",
            "Effect": "Allow",
            "Resource": "*",
            "Principal": "*"
        }
    ]
}
POLICY
}

data "external" "pwd" {
  program = ["bash", "./getpwd"]
}


#------------Create Subnet Associations---------------

# Public subnet 1 Association with Public Route Table
resource "aws_route_table_association" "public_assoc_1" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_rt.id
}

# Public subnet 2 Association with Public Route Table
resource "aws_route_table_association" "public_assoc_2" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_rt.id
}
# Private subnet 1 Association with Private Route Table
resource "aws_route_table_association" "private_assoc_1" {
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_default_route_table.private_rt.id
}

# Private subnet 2 Association with Private Route Table
resource "aws_route_table_association" "private_assoc_2" {
  subnet_id      = aws_subnet.private_subnet_2.id
  route_table_id = aws_default_route_table.private_rt.id
}

# Private subnet 2 Association with Private Route Table
resource "aws_route_table_association" "rds_assoc_1" {
  subnet_id      = aws_subnet.rds_subnet_1.id
  route_table_id = aws_default_route_table.private_rt.id
}

# Private subnet 2 Association with Private Route Table
resource "aws_route_table_association" "rds_assoc_2" {
  subnet_id      = aws_subnet.rds_subnet_2.id
  route_table_id = aws_default_route_table.private_rt.id
}

# Private subnet 2 Association with Private Route Table
resource "aws_route_table_association" "rds_assoc_3" {
  subnet_id      = aws_subnet.rds_subnet_3.id
  route_table_id = aws_default_route_table.private_rt.id
}

#------------Create RDS Subnet group---------------
#Defining which subnets should be used for RDS autoscaling_groups

resource "aws_db_subnet_group" "rds_subnetgroup" {
  name = var.db_subnet_group_name

  subnet_ids = ["${aws_subnet.rds_subnet_1.id}",
    "${aws_subnet.rds_subnet_2.id}",
    "${aws_subnet.rds_subnet_3.id}",
  ]

  tags = {
    Name = var.db_subnet_group_name
  }
}


#---------Add key pair key to bucket------
#resource "aws_s3_bucket_object" "key" {
#  bucket = var.s3_bucket
#  key    = "${var.key}.pem"
#  source = "${data.external.pwd.result.dir}/${var.key}"

#  etag = filemd5("${data.external.pwd.result.dir}/${var.key}")
#}
