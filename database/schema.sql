CREATE DATABASE IF NOT EXISTS toolkits;

USE toolkits;

CREATE TABLE IF NOT EXISTS users (
  user_id INT AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  INDEX(last_name),
  email VARCHAR(70)
);

CREATE TABLE IF NOT EXISTS languages (
  language_id INT AUTO_INCREMENT PRIMARY KEY,
  language_name VARCHAR(20)
);

CREATE TABLE IF NOT EXISTS tags (
  tag_id INT AUTO_INCREMENT PRIMARY KEY,
  tag VARCHAR(20)
);

CREATE TABLE IF NOT EXISTS git (
  code_id INT AUTO_INCREMENT PRIMARY KEY,
  owner_id INT,
  git_url VARCHAR(2083),
  description VARCHAR(240),
  FOREIGN KEY (owner_id) REFERENCES users(user_id)
);

CREATE TABLE IF NOT EXISTS contributors (
  code_id INT,
  contributor_id INT,
  FOREIGN KEY (contributor_id) REFERENCES users(user_id),
  FOREIGN KEY (code_id) REFERENCES git(code_id)
);

CREATE TABLE IF NOT EXISTS code_languages (
  code_id INT,
  language_id INT,
  FOREIGN KEY (language_id) REFERENCES languages(language_id),
  FOREIGN KEY (code_id) REFERENCES git(code_id)
);

CREATE TABLE IF NOT EXISTS code_tags (
  code_id INT,
  tag_id INT,
  FOREIGN KEY (tag_id) REFERENCES tags(tag_id),
  FOREIGN KEY (code_id) REFERENCES git(code_id)
);
