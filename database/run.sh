#!/bin/bash

docker build -t fantomdb:latest -f Dockerfile.DB .
docker run -itd --name FantomDB -p 32771:3306 -e MYSQL_ROOT_PASSWORD=secret123 fantomdb
if [ $1 = 'test' ]; then
  echo "Initiating test data"
  sleep 10
  mysql -h 127.0.0.1 -P 32771 -u root -psecret123 <test_data.sql
fi
