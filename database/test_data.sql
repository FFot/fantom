use toolkits;

INSERT IGNORE INTO users (first_name, last_name, email) VALUES ('Spencer', 'McSwizzle', 'spizzle@swizzle.com');
INSERT IGNORE INTO users (first_name, last_name, email) VALUES ('Stuart', 'McSwizzle', 'stizzle@swizzle.com');
INSERT IGNORE INTO users (first_name, last_name, email) VALUES ('Sharon', 'McSwizzle', 'shizzle@swizzle.com');

INSERT IGNORE INTO languages (language_name) VALUES ('French');
INSERT IGNORE INTO languages (language_name) VALUES ('German');
INSERT IGNORE INTO languages (language_name) VALUES ('Spanish');

INSERT IGNORE INTO tags (tag) VALUES ('It');
INSERT IGNORE INTO tags (tag) VALUES ('Tag');
INSERT IGNORE INTO tags (tag) VALUES ('Tig');

INSERT IGNORE INTO git (owner_id, git_url, description) SELECT users.user_id, 'example.git.com', 'Just test data' FROM users WHERE first_name = 'Spencer' AND last_name = 'McSwizzle';
INSERT IGNORE INTO git (git_url, description, owner_id) SELECT 'another.git.com', 'Just some more test data', users.user_id FROM users WHERE first_name = 'Sharon' AND last_name = 'McSwizzle';

INSERT IGNORE INTO contributors (code_id, contributor_id) SELECT git.code_id, users.user_id FROM git, users WHERE git.git_url = 'example.git.com' AND users.first_name = 'Stuart' AND users.last_name = 'McSwizzle';

INSERT IGNORE INTO code_languages (code_id, language_id) SELECT git.code_id, languages.language_id FROM git, languages WHERE git.git_url = 'example.git.com' AND languages.language_name = 'French';
INSERT IGNORE INTO code_languages (code_id, language_id) SELECT git.code_id, languages.language_id FROM git, languages WHERE git.git_url = 'another.git.com' AND languages.language_name = 'German';

INSERT IGNORE INTO code_tags (code_id, tag_id) SELECT git.code_id, tags.tag_id FROM git, tags WHERE git.git_url = 'example.git.com' AND tags.tag = 'Tag';
INSERT IGNORE INTO code_tags (code_id, tag_id) SELECT git.code_id, tags.tag_id FROM git, tags WHERE git.git_url = 'another.git.com' AND tags.tag = 'Tig';
