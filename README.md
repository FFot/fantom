# README #

Deploying a full infrastructure for the codejar project

### What you need first ###

* AWS_CLI with credentials
* An S3 Bucket
* terraform
* Docker


### How to deploy ###

* Change variables in terraform.tfvars and variables.txt
* They are duplicates in the two files, these MUST match
* Run ./run_all.sh from the fantom directory location

### This is the work of Fantom ###

* Fani Foteva: fani@automationlogic.com
* Tom Moran: thomas.moran@automationlogic.com
